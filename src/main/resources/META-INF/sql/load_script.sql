create table if not exists role_permissions(role varchar(100), permission varchar(100), inited_flag char(1) default 'N');

-- The inited_flag column is a little hack because mySQL does not support INSERT ... WHERE NOT EXISTS ...
-- But we need to check if the table is new or already initialized. Else we may overwrite the values at each server startup.
-- You can remove the inited_flag column if you do not use this init script, anymore.

INSERT INTO role_permissions (role, permission) SELECT 'Administrator','item:delete' FROM dual WHERE NOT EXISTS (SELECT * FROM role_permissions WHERE inited_flag = 'Y');

INSERT INTO role_permissions (role, permission) SELECT 'Registered','item:delete' FROM dual WHERE NOT EXISTS (SELECT * FROM role_permissions WHERE inited_flag = 'Y');

INSERT INTO role_permissions (role, permission) SELECT 'Administrator','attachement:update' FROM dual WHERE NOT EXISTS (SELECT * FROM role_permissions WHERE inited_flag = 'Y');

INSERT INTO role_permissions (role, permission) SELECT 'Registered','attachement:update' FROM dual WHERE NOT EXISTS (SELECT * FROM role_permissions WHERE inited_flag = 'Y');

INSERT INTO role_permissions (role, permission) SELECT 'Administrator','user:*' FROM dual WHERE NOT EXISTS (SELECT * FROM role_permissions WHERE inited_flag = 'Y');

UPDATE role_permissions set inited_flag = 'Y';

package org.application.domain;

public enum ItemType {

    FOOD, BEVERAGES, MACHINE;
}

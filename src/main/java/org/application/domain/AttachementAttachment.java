package org.application.domain;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="AttachementAttachment")
public class AttachementAttachment extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public AttachementAttachment() {
        super();
    }
    
    public AttachementAttachment(Long id, String fileName) {
        this.setId(id);
        this.fileName = fileName;
    }

    @Size(max = 200)
    private String fileName;
    
    @ManyToOne
    @JoinColumn(name = "ATTACHEMENT_ID", referencedColumnName = "ID")
    private AttachementEntity attachement;

    @Lob
    private byte[] content;

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
    
    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public AttachementEntity getAttachement() {
        return this.attachement;
    }

    public void setAttachement(AttachementEntity attachement) {
        this.attachement = attachement;
    }
}

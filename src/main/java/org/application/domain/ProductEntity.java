package org.application.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="Product")
@Table(name="\"PRODUCT\"")
@XmlRootElement
public class ProductEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 50)
    @Column(length = 50, name="\"name\"")
    @NotNull
    private String name;

    @Digits(integer = 5,  fraction = 2)
    @Column(precision = 7, scale = 2, name="\"price\"")
    private BigDecimal price;

    @Column(name="\"stock\"")
    @Digits(integer = 4, fraction = 0)
    private Integer stock;

    @Column(name="\"launchDate\"")
    @Temporal(TemporalType.DATE)
    private Date launchDate;

    @Column(name="\"discontinued\"")
    private Boolean discontinued;

    @ManyToOne(optional=true)
    @JoinColumn(name = "ITEMS_ID", referencedColumnName = "ID")
    private ItemEntity items;

    @ManyToOne(optional=true)
    @JoinColumn(name = "ATTACHEMENT_ID", referencedColumnName = "ID")
    private AttachementEntity attachement;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStock() {
        return this.stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Date getLaunchDate() {
        return this.launchDate;
    }

    public void setLaunchDate(Date launchDate) {
        this.launchDate = launchDate;
    }

    public Boolean getDiscontinued() {
        return this.discontinued;
    }

    public void setDiscontinued(Boolean discontinued) {
        this.discontinued = discontinued;
    }

    public ItemEntity getItems() {
        return this.items;
    }

    public void setItems(ItemEntity item) {
        this.items = item;
    }

    public AttachementEntity getAttachement() {
        return this.attachement;
    }

    public void setAttachement(AttachementEntity attachement) {
        this.attachement = attachement;
    }

}

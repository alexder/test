package org.application.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="Item")
@Table(name="\"ITEM\"")
@XmlRootElement
public class ItemEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name="\"date\"")
    @Temporal(TemporalType.DATE)
    private Date date;

    @ElementCollection(targetClass = ItemType.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "ITEM_TYPE", joinColumns = { @JoinColumn(name = "ITEM_ID") })
    @Column(name = "\"TYPE\"")
    private Set<ItemType> type;

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<ItemType> getType() {
        return type;
    }

    public void setType(Set<ItemType> type) {
        this.type = type;
    }

}

package org.application.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.application.domain.ItemEntity;
import org.application.domain.ItemType;
import org.application.domain.ProductEntity;
import org.application.service.ItemService;
import org.application.service.ProductService;
import org.application.service.security.SecurityWrapper;
import org.application.web.util.MessageFactory;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

@Named("itemBean")
@ViewScoped
public class ItemBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(ItemBean.class.getName());
    
    private List<ItemEntity> itemList;

    private ItemEntity item;
    
    @Inject
    private ItemService itemService;
    
    @Inject
    private ProductService productService;
    
    private DualListModel<ProductEntity> products;
    private List<String> transferedProductIDs;
    private List<String> removedProductIDs;
    
    public void prepareNewItem() {
        reset();
        this.item = new ItemEntity();
        // set any default values now, if you need
        // Example: this.item.setAnything("test");
    }

    public String persist() {

        if (item.getId() == null && !isPermitted("item:create")) {
            return "accessDenied";
        } else if (item.getId() != null && !isPermitted(item, "item:update")) {
            return "accessDenied";
        }
        
        String message = "";
        
        try {
            
            if (item.getId() != null) {
                item = itemService.update(item);
                message = "message_successfully_updated";
            } else {
                item = itemService.save(item);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        itemList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        
        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(item, "item:delete")) {
            return "accessDenied";
        }
        
        String message = "";
        
        try {
            itemService.delete(item);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void reset() {
        item = null;
        itemList = null;
        
        products = null;
        transferedProductIDs = null;
        removedProductIDs = null;
        
    }

    public DualListModel<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(DualListModel<ProductEntity> products) {
        this.products = products;
    }
    
    public List<ProductEntity> getFullProductsList() {
        List<ProductEntity> allList = new ArrayList<ProductEntity>();
        allList.addAll(products.getSource());
        allList.addAll(products.getTarget());
        return allList;
    }
    
    public void onProductsDialog(ItemEntity item) {
        // Prepare the product PickList
        this.item = item;
        List<ProductEntity> selectedProductsFromDB = productService
                .findProductsByItems(this.item);
        List<ProductEntity> availableProductsFromDB = productService
                .findAvailableProducts(this.item);
        this.products = new DualListModel<>(availableProductsFromDB, selectedProductsFromDB);
        
        transferedProductIDs = new ArrayList<String>();
        removedProductIDs = new ArrayList<String>();
    }
    
    public void onProductsPickListTransfer(TransferEvent event) {
        // If a product is transferred within the PickList, we just transfer it in this
        // bean scope. We do not change anything it the database, yet.
        for (Object item : event.getItems()) {
            String id = ((ProductEntity) item).getId().toString();
            if (event.isAdd()) {
                transferedProductIDs.add(id);
                removedProductIDs.remove(id);
            } else if (event.isRemove()) {
                removedProductIDs.add(id);
                transferedProductIDs.remove(id);
            }
        }
        
    }
    
    public void updateProduct(ProductEntity product) {
        // If a new product is created, we persist it to the database,
        // but we do not assign it to this item in the database, yet.
        products.getTarget().add(product);
        transferedProductIDs.add(product.getId().toString());
    }
    
    public void onProductsSubmit() {
        // Now we save the changed of the PickList to the database.
        try {
            List<ProductEntity> selectedProductsFromDB = productService
                    .findProductsByItems(this.item);
            List<ProductEntity> availableProductsFromDB = productService
                    .findAvailableProducts(this.item);
            
            for (ProductEntity product : selectedProductsFromDB) {
                if (removedProductIDs.contains(product.getId().toString())) {
                    product.setItems(null);
                    productService.update(product);
                }
            }
    
            for (ProductEntity product : availableProductsFromDB) {
                if (transferedProductIDs.contains(product.getId().toString())) {
                    product.setItems(item);
                    productService.update(product);
                }
            }
            
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_changes_saved");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            reset();

        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_optimistic_locking_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_picklist_save_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }
    
    public SelectItem[] getTypeSelectItems() {
        SelectItem[] items = new SelectItem[ItemType.values().length];

        int i = 0;
        for (ItemType type : ItemType.values()) {
            items[i++] = new SelectItem(type, getLabelForType(type));
        }
        return items;
    }
    
    public String getLabelForType(ItemType value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_item_type_" + value);
        return label == null? value.toString() : label;
    }
    
    public ItemEntity getItem() {
        if (this.item == null) {
            prepareNewItem();
        }
        return this.item;
    }
    
    public void setItem(ItemEntity item) {
        this.item = item;
    }
    
    public List<ItemEntity> getItemList() {
        if (itemList == null) {
            itemList = itemService.findAllItemEntities();
        }
        return itemList;
    }

    public void setItemList(List<ItemEntity> itemList) {
        this.itemList = itemList;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(ItemEntity item, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}

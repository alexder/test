package org.application.web;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.apache.commons.io.IOUtils;
import org.application.domain.AttachementAttachment;
import org.application.domain.AttachementEntity;
import org.application.domain.AttachementImage;
import org.application.domain.ProductEntity;
import org.application.service.AttachementAttachmentService;
import org.application.service.AttachementService;
import org.application.service.ProductService;
import org.application.service.security.SecurityWrapper;
import org.application.web.util.MessageFactory;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

@Named("attachementBean")
@ViewScoped
public class AttachementBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(AttachementBean.class.getName());
    
    private List<AttachementEntity> attachementList;

    private AttachementEntity attachement;
    
    private List<AttachementAttachment> attachementAttachments;
    
    @Inject
    private AttachementService attachementService;
    
    @Inject
    private AttachementAttachmentService attachementAttachmentService;
    
    UploadedFile uploadedImage;
    byte[] uploadedImageContents;
    
    @Inject
    private ProductService productService;
    
    private DualListModel<ProductEntity> products;
    private List<String> transferedProductIDs;
    private List<String> removedProductIDs;
    
    public void prepareNewAttachement() {
        reset();
        this.attachement = new AttachementEntity();
        // set any default values now, if you need
        // Example: this.attachement.setAnything("test");
    }

    public String persist() {

        if (attachement.getId() == null && !isPermitted("attachement:create")) {
            return "accessDenied";
        } else if (attachement.getId() != null && !isPermitted(attachement, "attachement:update")) {
            return "accessDenied";
        }
        
        String message = "";
        
        try {
            
            if (this.uploadedImage != null) {
                try {

                    InputStream in = new ByteArrayInputStream(uploadedImageContents);
                    BufferedImage image = ImageIO.read(in);
                    in.close();
                    image = Scalr.resize(image, Method.BALANCED, 300);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageOutputStream imageOS = ImageIO.createImageOutputStream(baos);
                    Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                            uploadedImage.getContentType());
                    ImageWriter imageWriter = (ImageWriter) imageWriters.next();
                    imageWriter.setOutput(imageOS);
                    imageWriter.write(image);
                    
                    baos.close();
                    imageOS.close();
                    
                    logger.log(Level.INFO, "Resized uploaded image from "
                            + uploadedImageContents.length + " to " + baos.toByteArray().length);
            
                    AttachementImage attachementImage = new AttachementImage();
                    attachementImage.setContent(baos.toByteArray());
                    attachement.setImage(attachementImage);
                } catch (Exception e) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "message_upload_exception");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return null;
                }
            }
            
            if (attachement.getId() != null) {
                attachement = attachementService.update(attachement);
                message = "message_successfully_updated";
            } else {
                attachement = attachementService.save(attachement);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        attachementList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        
        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(attachement, "attachement:delete")) {
            return "accessDenied";
        }
        
        String message = "";
        
        try {
            attachementService.delete(attachement);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void reset() {
        attachement = null;
        attachementList = null;
        
        attachementAttachments = null;
        
        products = null;
        transferedProductIDs = null;
        removedProductIDs = null;
        
        uploadedImage = null;
        uploadedImageContents = null;
        
    }

    public DualListModel<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(DualListModel<ProductEntity> products) {
        this.products = products;
    }
    
    public List<ProductEntity> getFullProductsList() {
        List<ProductEntity> allList = new ArrayList<ProductEntity>();
        allList.addAll(products.getSource());
        allList.addAll(products.getTarget());
        return allList;
    }
    
    public void onProductsDialog(AttachementEntity attachement) {
        // Prepare the product PickList
        this.attachement = attachement;
        List<ProductEntity> selectedProductsFromDB = productService
                .findProductsByAttachement(this.attachement);
        List<ProductEntity> availableProductsFromDB = productService
                .findAvailableProducts(this.attachement);
        this.products = new DualListModel<>(availableProductsFromDB, selectedProductsFromDB);
        
        transferedProductIDs = new ArrayList<String>();
        removedProductIDs = new ArrayList<String>();
    }
    
    public void onProductsPickListTransfer(TransferEvent event) {
        // If a product is transferred within the PickList, we just transfer it in this
        // bean scope. We do not change anything it the database, yet.
        for (Object item : event.getItems()) {
            String id = ((ProductEntity) item).getId().toString();
            if (event.isAdd()) {
                transferedProductIDs.add(id);
                removedProductIDs.remove(id);
            } else if (event.isRemove()) {
                removedProductIDs.add(id);
                transferedProductIDs.remove(id);
            }
        }
        
    }
    
    public void updateProduct(ProductEntity product) {
        // If a new product is created, we persist it to the database,
        // but we do not assign it to this attachement in the database, yet.
        products.getTarget().add(product);
        transferedProductIDs.add(product.getId().toString());
    }
    
    public void onProductsSubmit() {
        // Now we save the changed of the PickList to the database.
        try {
            List<ProductEntity> selectedProductsFromDB = productService
                    .findProductsByAttachement(this.attachement);
            List<ProductEntity> availableProductsFromDB = productService
                    .findAvailableProducts(this.attachement);
            
            for (ProductEntity product : selectedProductsFromDB) {
                if (removedProductIDs.contains(product.getId().toString())) {
                    product.setAttachement(null);
                    productService.update(product);
                }
            }
    
            for (ProductEntity product : availableProductsFromDB) {
                if (transferedProductIDs.contains(product.getId().toString())) {
                    product.setAttachement(attachement);
                    productService.update(product);
                }
            }
            
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_changes_saved");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            
            reset();

        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_optimistic_locking_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_picklist_save_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }
    
    public void handleImageUpload(FileUploadEvent event) {
        
        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                event.getFile().getContentType());
        if (!imageWriters.hasNext()) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_image_type_not_supported");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return;
        }
        
        this.uploadedImage = event.getFile();
        this.uploadedImageContents = event.getFile().getContents();
        
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public byte[] getUploadedImageContents() {
        if (uploadedImageContents != null) {
            return uploadedImageContents;
        } else if (attachement != null && attachement.getImage() != null) {
            attachement = attachementService.lazilyLoadImageToAttachement(attachement);
            return attachement.getImage().getContent();
        }
        return null;
    }
    
    public List<AttachementAttachment> getAttachementAttachments() {
        if (this.attachementAttachments == null && this.attachement != null && this.attachement.getId() != null) {
            // The byte streams are not loaded from database with following line. This would cost too much.
            this.attachementAttachments = this.attachementAttachmentService.getAttachmentsList(attachement);
        }
        return this.attachementAttachments;
    }
    
    public void handleAttachmentUpload(FileUploadEvent event) {
        
        AttachementAttachment attachementAttachment = new AttachementAttachment();
        
        try {
            // Would be better to use ...getFile().getContents(), but does not work on every environment
            attachementAttachment.setContent(IOUtils.toByteArray(event.getFile().getInputstream()));
        
            attachementAttachment.setFileName(event.getFile().getFileName());
            attachementAttachment.setAttachement(attachement);
            attachementAttachmentService.save(attachementAttachment);
            
            // set attachementAttachment to null, will be refreshed on next demand
            this.attachementAttachments = null;
            
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_successfully_uploaded");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_upload_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }

    public StreamedContent getAttachmentStreamedContent(
            AttachementAttachment attachementAttachment) {
        if (attachementAttachment != null && attachementAttachment.getContent() == null) {
            attachementAttachment = attachementAttachmentService
                    .find(attachementAttachment.getId());
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(
                attachementAttachment.getContent()),
                new MimetypesFileTypeMap().getContentType(attachementAttachment
                        .getFileName()), attachementAttachment.getFileName());
    }

    public String deleteAttachment(AttachementAttachment attachment) {
        
        attachementAttachmentService.delete(attachment);
        
        // set attachementAttachment to null, will be refreshed on next demand
        this.attachementAttachments = null;
        
        FacesMessage facesMessage = MessageFactory.getMessage(
                "message_successfully_deleted", "Attachment");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        return null;
    }
    
    public AttachementEntity getAttachement() {
        if (this.attachement == null) {
            prepareNewAttachement();
        }
        return this.attachement;
    }
    
    public void setAttachement(AttachementEntity attachement) {
        this.attachement = attachement;
    }
    
    public List<AttachementEntity> getAttachementList() {
        if (attachementList == null) {
            attachementList = attachementService.findAllAttachementEntities();
        }
        return attachementList;
    }

    public void setAttachementList(List<AttachementEntity> attachementList) {
        this.attachementList = attachementList;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(AttachementEntity attachement, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}

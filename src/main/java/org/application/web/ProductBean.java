package org.application.web;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.application.domain.ItemEntity;
import org.application.domain.ProductEntity;
import org.application.service.ItemService;
import org.application.service.ProductService;
import org.application.service.security.SecurityWrapper;
import org.application.web.generic.GenericLazyDataModel;
import org.application.web.util.MessageFactory;

@Named("productBean")
@ViewScoped
public class ProductBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(ProductBean.class.getName());
    
    private GenericLazyDataModel<ProductEntity> lazyModel;
    
    private ProductEntity product;
    
    @Inject
    private ProductService productService;
    
    @Inject
    private ItemService itemService;
    
    private List<ItemEntity> allItemssList;
    
    public void prepareNewProduct() {
        reset();
        this.product = new ProductEntity();
        // set any default values now, if you need
        // Example: this.product.setAnything("test");
    }

    public GenericLazyDataModel<ProductEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<ProductEntity>(productService);
        }
        return this.lazyModel;
    }
    
    public String persist() {

        String message = "";
        
        try {
            
            if (product.getId() != null) {
                product = productService.update(product);
                message = "message_successfully_updated";
            } else {
                product = productService.save(product);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        
        return null;
    }
    
    public String delete() {
        
        String message = "";
        
        try {
            productService.delete(product);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void reset() {
        product = null;

        allItemssList = null;
        
    }

    // Get a List of all items
    public List<ItemEntity> getItemss() {
        if (this.allItemssList == null) {
            this.allItemssList = itemService.findAllItemEntities();
        }
        return this.allItemssList;
    }
    
    // Update items of the current product
    public void updateItems(ItemEntity item) {
        this.product.setItems(item);
        // Maybe we just created and assigned a new item. So reset the allItemsList.
        allItemssList = null;
    }
    
    public ProductEntity getProduct() {
        if (this.product == null) {
            prepareNewProduct();
        }
        return this.product;
    }
    
    public void setProduct(ProductEntity product) {
        this.product = product;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(ProductEntity product, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}

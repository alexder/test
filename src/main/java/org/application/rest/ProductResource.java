package org.application.rest;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.application.domain.ProductEntity;
import org.application.service.ProductService;

@Path("/products")
@Named
public class ProductResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private ProductService productService;
    
    /**
     * Get the complete list of Product Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /products
     * @return List of ProductEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductEntity> getAllProducts() {
        return productService.findAllProductEntities();
    }
    
    /**
     * Get the number of Product Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /products/count
     * @return Number of ProductEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return productService.countAllEntries();
    }
    
    /**
     * Get a Product Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /products/3
     * @return A Product Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ProductEntity getProductById(@PathParam("id") Long id) {
        return productService.find(id);
    }
    
    /**
     * Create a Product Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New ProductEntity (JSON) <br/>
     * Example URL: /products
     * @return A ProductEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductEntity addProduct(ProductEntity product) {
        return productService.save(product);
    }
    
    /**
     * Update an existing Product Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated ProductEntity (JSON) <br/>
     * Example URL: /products
     * @return A ProductEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ProductEntity updateProduct(ProductEntity product) {
        return productService.update(product);
    }
    
    /**
     * Delete an existing Product Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /products/3
     */
    @Path("{id}")
    @DELETE
    public void deleteProduct(@PathParam("id") Long id) {
        ProductEntity product = productService.find(id);
        productService.delete(product);
    }
    
}

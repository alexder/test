package org.application.rest;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.application.domain.ItemEntity;
import org.application.domain.ProductEntity;
import org.application.service.ItemService;
import org.application.service.ProductService;

@Path("/items")
@Named
public class ItemResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private ItemService itemService;
    
    @Inject
    private ProductService productService;
    
    /**
     * Get the complete list of Item Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /items
     * @return List of ItemEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ItemEntity> getAllItems() {
        return itemService.findAllItemEntities();
    }
    
    /**
     * Get the number of Item Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /items/count
     * @return Number of ItemEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return itemService.countAllEntries();
    }
    
    /**
     * Get a Item Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /items/3
     * @return A Item Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ItemEntity getItemById(@PathParam("id") Long id) {
        return itemService.find(id);
    }
    
    /**
     * Create a Item Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New ItemEntity (JSON) <br/>
     * Example URL: /items
     * @return A ItemEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ItemEntity addItem(ItemEntity item) {
        return itemService.save(item);
    }
    
    /**
     * Update an existing Item Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated ItemEntity (JSON) <br/>
     * Example URL: /items
     * @return A ItemEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ItemEntity updateItem(ItemEntity item) {
        return itemService.update(item);
    }
    
    /**
     * Delete an existing Item Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /items/3
     */
    @Path("{id}")
    @DELETE
    public void deleteItem(@PathParam("id") Long id) {
        ItemEntity item = itemService.find(id);
        itemService.delete(item);
    }
    
    /**
     * Get the list of Product that is assigned to a Item <br/>
     * HTTP Method: GET <br/>
     * Example URL: /items/3/products
     * @return List of ProductEntity
     */
    @GET
    @Path("{id}/products")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductEntity> getProducts(@PathParam("id") Long itemId) {
        ItemEntity item = itemService.find(itemId);
        return productService.findProductsByItems(item);
    }
    
    /**
     * Assign an existing Product to an existing Item <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: empty <br/>
     * Example URL: /items/3/products/8
     */
    @PUT
    @Path("{id}/products/{productId}")
    public void assignProduct(@PathParam("id") Long itemId, @PathParam("productId") Long productId) {
        ItemEntity item = itemService.find(itemId);
        ProductEntity product = productService.find(productId);
        product.setItems(item);
        productService.update(product);
    }
    
    /**
     * Remove a Item-to-Product Assignment <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /items/3/products/8
     */
    @DELETE
    @Path("{id}/products/{productId}")
    public void unassignProduct(@PathParam("id") Long itemId, @PathParam("productId") Long productId) {
        ProductEntity product = productService.find(productId);
        product.setItems(null);
        productService.update(product);
    }
    
}

package org.application.rest;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.application.domain.AttachementEntity;
import org.application.domain.ProductEntity;
import org.application.service.AttachementService;
import org.application.service.ProductService;

@Path("/attachements")
@Named
public class AttachementResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private AttachementService attachementService;
    
    @Inject
    private ProductService productService;
    
    /**
     * Get the complete list of Attachement Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /attachements
     * @return List of AttachementEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<AttachementEntity> getAllAttachements() {
        return attachementService.findAllAttachementEntities();
    }
    
    /**
     * Get the number of Attachement Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /attachements/count
     * @return Number of AttachementEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return attachementService.countAllEntries();
    }
    
    /**
     * Get a Attachement Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /attachements/3
     * @return A Attachement Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public AttachementEntity getAttachementById(@PathParam("id") Long id) {
        return attachementService.find(id);
    }
    
    /**
     * Create a Attachement Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New AttachementEntity (JSON) <br/>
     * Example URL: /attachements
     * @return A AttachementEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AttachementEntity addAttachement(AttachementEntity attachement) {
        return attachementService.save(attachement);
    }
    
    /**
     * Update an existing Attachement Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated AttachementEntity (JSON) <br/>
     * Example URL: /attachements
     * @return A AttachementEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AttachementEntity updateAttachement(AttachementEntity attachement) {
        return attachementService.update(attachement);
    }
    
    /**
     * Delete an existing Attachement Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /attachements/3
     */
    @Path("{id}")
    @DELETE
    public void deleteAttachement(@PathParam("id") Long id) {
        AttachementEntity attachement = attachementService.find(id);
        attachementService.delete(attachement);
    }
    
    /**
     * Get the list of Product that is assigned to a Attachement <br/>
     * HTTP Method: GET <br/>
     * Example URL: /attachements/3/products
     * @return List of ProductEntity
     */
    @GET
    @Path("{id}/products")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProductEntity> getProducts(@PathParam("id") Long attachementId) {
        AttachementEntity attachement = attachementService.find(attachementId);
        return productService.findProductsByAttachement(attachement);
    }
    
    /**
     * Assign an existing Product to an existing Attachement <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: empty <br/>
     * Example URL: /attachements/3/products/8
     */
    @PUT
    @Path("{id}/products/{productId}")
    public void assignProduct(@PathParam("id") Long attachementId, @PathParam("productId") Long productId) {
        AttachementEntity attachement = attachementService.find(attachementId);
        ProductEntity product = productService.find(productId);
        product.setAttachement(attachement);
        productService.update(product);
    }
    
    /**
     * Remove a Attachement-to-Product Assignment <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /attachements/3/products/8
     */
    @DELETE
    @Path("{id}/products/{productId}")
    public void unassignProduct(@PathParam("id") Long attachementId, @PathParam("productId") Long productId) {
        ProductEntity product = productService.find(productId);
        product.setAttachement(null);
        productService.update(product);
    }
    
}

package org.application.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.application.domain.AttachementEntity;
import org.application.domain.ItemEntity;
import org.application.domain.ProductEntity;
import org.primefaces.model.SortOrder;

@Named
public class ProductService extends BaseService<ProductEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public ProductService(){
        super(ProductEntity.class);
    }
    
    @Transactional
    public List<ProductEntity> findAllProductEntities() {
        
        return entityManager.createQuery("SELECT o FROM Product o ", ProductEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Product o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(ProductEntity product) {

        /* This is called before a Product is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<ProductEntity> findAvailableProducts(ItemEntity item) {
        return entityManager.createQuery("SELECT o FROM Product o WHERE o.items IS NULL", ProductEntity.class).getResultList();
    }

    @Transactional
    public List<ProductEntity> findProductsByItems(ItemEntity item) {
        return entityManager.createQuery("SELECT o FROM Product o WHERE o.items = :item", ProductEntity.class).setParameter("item", item).getResultList();
    }

    @Transactional
    public List<ProductEntity> findAvailableProducts(AttachementEntity attachement) {
        return entityManager.createQuery("SELECT o FROM Product o WHERE o.attachement IS NULL", ProductEntity.class).getResultList();
    }

    @Transactional
    public List<ProductEntity> findProductsByAttachement(AttachementEntity attachement) {
        return entityManager.createQuery("SELECT o FROM Product o WHERE o.attachement = :attachement", ProductEntity.class).setParameter("attachement", attachement).getResultList();
    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<ProductEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        
        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Product o");
        
        // Can be optimized: We need this join only when items filter is set
        query.append(" LEFT OUTER JOIN o.items items");
        
        // Can be optimized: We need this join only when attachement filter is set
        query.append(" LEFT OUTER JOIN o.attachement attachement");
        
        String nextConnective = " WHERE";
        
        Map<String, Object> queryParameters = new HashMap<String, Object>();
        
        if (filters != null && !filters.isEmpty()) {
            
            nextConnective += " ( ";
            
            for(String filterProperty : filters.keySet()) {
                
                if (filters.get(filterProperty) == null) {
                    continue;
                }
                
                switch (filterProperty) {
                
                case "name":
                    query.append(nextConnective).append(" o.name LIKE :name");
                    queryParameters.put("name", "%" + filters.get(filterProperty) + "%");
                    break;

                case "price":
                    query.append(nextConnective).append(" o.price = :price");
                    queryParameters.put("price", new BigDecimal(filters.get(filterProperty).toString()));
                    break;

                case "stock":
                    query.append(nextConnective).append(" o.stock = :stock");
                    queryParameters.put("stock", new Integer(filters.get(filterProperty).toString()));
                    break;

                case "launchDate":
                    query.append(nextConnective).append(" o.launchDate = :launchDate");
                    queryParameters.put("launchDate", filters.get(filterProperty));
                    break;

                case "discontinued":
                    query.append(nextConnective).append(" o.discontinued = :discontinued");
                    queryParameters.put("discontinued", filters.get(filterProperty));
                    break;

                case "items":
                    query.append(nextConnective).append(" o.items = :items");
                    queryParameters.put("items", filters.get(filterProperty));
                    break;
                
                case "attachement":
                    query.append(nextConnective).append(" o.attachement = :attachement");
                    queryParameters.put("attachement", filters.get(filterProperty));
                    break;
                
                }
                
                nextConnective = " AND";
            }
            
            query.append(" ) ");
            nextConnective = " AND";
        }
        
        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }
        
        TypedQuery<ProductEntity> q = this.entityManager.createQuery(query.toString(), this.getType());
        
        for(String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}

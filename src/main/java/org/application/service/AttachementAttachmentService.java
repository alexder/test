package org.application.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

import org.application.domain.AttachementAttachment;
import org.application.domain.AttachementEntity;

@Named
public class AttachementAttachmentService extends BaseService<AttachementAttachment> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public AttachementAttachmentService(){
        super(AttachementAttachment.class);
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM AttachementAttachment o", Long.class).getSingleResult();
    }

    @Transactional
    public void deleteAttachmentsByAttachement(AttachementEntity attachement) {
        entityManager
                .createQuery("DELETE FROM AttachementAttachment c WHERE c.attachement = :p")
                .setParameter("p", attachement).executeUpdate();
    }
    
    @Transactional
    public List<AttachementAttachment> getAttachmentsList(AttachementEntity attachement) {
        if (attachement == null || attachement.getId() == null) {
            return new ArrayList<AttachementAttachment>();
        }
        // The byte streams are not loaded from database with following line. This would cost too much.
        return entityManager.createQuery("SELECT NEW org.application.domain.AttachementAttachment(o.id, o.fileName) FROM AttachementAttachment o WHERE o.attachement.id = :id", AttachementAttachment.class).setParameter("id", attachement.getId()).getResultList();
    }
}

package org.application.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceUnitUtil;
import javax.transaction.Transactional;

import org.application.domain.AttachementEntity;
import org.application.service.security.SecurityWrapper;

@Named
public class AttachementService extends BaseService<AttachementEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public AttachementService(){
        super(AttachementEntity.class);
    }
    
    @Inject
    private AttachementAttachmentService attachmentService;
    
    @Transactional
    public List<AttachementEntity> findAllAttachementEntities() {
        
        return entityManager.createQuery("SELECT o FROM Attachement o LEFT JOIN FETCH o.image", AttachementEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Attachement o", Long.class).getSingleResult();
    }
    
    @Override
    @Transactional
    public AttachementEntity save(AttachementEntity attachement) {
        String username = SecurityWrapper.getUsername();
        
        attachement.updateAuditInformation(username);
        
        return super.save(attachement);
    }
    
    @Override
    @Transactional
    public AttachementEntity update(AttachementEntity attachement) {
        String username = SecurityWrapper.getUsername();
        attachement.updateAuditInformation(username);
        return super.update(attachement);
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(AttachementEntity attachement) {

        /* This is called before a Attachement is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.attachmentService.deleteAttachmentsByAttachement(attachement);
        
        this.cutAllAttachementProductsAssignments(attachement);
        
    }

    // Remove all assignments from all product a attachement. Called before delete a attachement.
    @Transactional
    private void cutAllAttachementProductsAssignments(AttachementEntity attachement) {
        entityManager
                .createQuery("UPDATE Product c SET c.attachement = NULL WHERE c.attachement = :p")
                .setParameter("p", attachement).executeUpdate();
    }
    
    @Transactional
    public AttachementEntity lazilyLoadImageToAttachement(AttachementEntity attachement) {
        PersistenceUnitUtil u = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        if (!u.isLoaded(attachement, "image") && attachement.getId() != null) {
            attachement = find(attachement.getId());
            attachement.getImage().getId();
        }
        return attachement;
    }
    
}

package org.application.service;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

import org.application.domain.ItemEntity;

@Named
public class ItemService extends BaseService<ItemEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public ItemService(){
        super(ItemEntity.class);
    }
    
    @Transactional
    public List<ItemEntity> findAllItemEntities() {
        
        return entityManager.createQuery("SELECT o FROM Item o ", ItemEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Item o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(ItemEntity item) {

        /* This is called before a Item is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllItemsProductsAssignments(item);
        
    }

    // Remove all assignments from all product a item. Called before delete a item.
    @Transactional
    private void cutAllItemsProductsAssignments(ItemEntity item) {
        entityManager
                .createQuery("UPDATE Product c SET c.items = NULL WHERE c.items = :p")
                .setParameter("p", item).executeUpdate();
    }
    
}
